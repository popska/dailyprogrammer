[source](https://www.reddit.com/r/dailyprogrammer/comments/nzmvsj/20210614_challenge_394_difficult_rsa_encryption/)

Implement the RSA key generation process following the specification on Wikipedia, or some other similar specification. Randomly generate 256-bit or larger values for p and q, using the Fermat primality test or something similar. Use e = 65537. Provide functions to encrypt and decrypt a whole number representing a message, using your selected n. Verify that when you encrypt and then decrypt the input 12345, you get 12345 back.


# Plan

* finding prime numbers *p* and *q*
    * use `secrets.randbits()` to generate a number, *n*, with 256 bits
        * if this number is even, add 1
    * use [fermat primality test](https://en.wikipedia.org/wiki/Fermat_primality_test) to check if the number is composite (with k = 64)
        * pick a random number, *a*, from (1 - n-1)
        * if `a^(n-1) % n != 1` then the number isn't prime
        * repeat k times
        * if otherwise, this is *probably* prime
            * note that [Carmichael numbers](https://en.wikipedia.org/wiki/Carmichael_number) may fail this test.  `¯\_(ツ)_/¯`

* compute the public key, `n = pq`
* calculate lambda(n) = lcm(p - 1, q - 1)
    * using gcd: lcm = |p * q| / gcd(p, q)
* use 65537 for *e*

* calculating modular inverse
    * apply euclidian algorithm to find `gcd(a, b)`
        * recursively calculate `gcd(b % a, a)`
        * stop and return b when `a == 0`
    * on way out of recursion calculate x & y
        * x = y1 - ⌊b/a⌋ * x1
        * y = x1
    * x is modular inverse

* encrypting message *m*
    * m^e % n
* decrypting message *c*
    * c^d % n

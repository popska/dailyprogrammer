import secrets
import math
import random

def generate_prime(bits):
    n = secrets.randbits(bits)
    if n % 2 == 0:
        n += 1
    while not is_prime(n):
        n = secrets.randbits(bits)
    return n

def is_prime(n, k=64):
    for _ in range(k):
        a = random.randrange(n)
        if pow(a, n-1, n) != 1:
            return False
    return True

def lcm(a, b):
    return abs(a * b) // math.gcd(a, b)

def modular_inverse(e, phi):
    g, x, y = extended_gcd(e, phi)
    if x < 0:  # the extendend euclidian alg can return negative d
        x += phi
    return x

def extended_gcd(a, b):
    if a == 0:
        return b, 0, 1
    gcd, x1, y1 = extended_gcd(b % a, a)
    x = y1 - (b // a) * x1
    y = x1
    return gcd, x, y

class RSA:
    def __init__(self, bits=256, e=65537):
        p = generate_prime(bits)
        q = generate_prime(bits)
        self.n = p * q  # public key
        phi = lcm(p - 1, q - 1)
        self.e = e
        self.d = modular_inverse(self.e, phi)  # private key

    def encrypt(self, m):
        return pow(m, self.e, self.n)

    def decrypt(self, c):
        return pow(c, self.d, self.n)

if __name__ == '__main__':
    rsa = RSA(256)

    message = 12345
    print("message: ", message)
    crypted = rsa.encrypt(message)
    print("encrypted: ", crypted)
    decrypted = rsa.decrypt(crypted)
    print("decrypted: ", decrypted)




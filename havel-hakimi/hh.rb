def hh(numbers)
  numbers.reject! {|n| n == 0}
  if numbers.empty?
    true
  else
    numbers.sort!.reverse!
    n = numbers.shift
    if n > numbers.length
      false
    else
      (0...n).each do |i|
        numbers[i] -= 1
      end
      hh(numbers)
    end
  end
end

puts hh([15, 18, 6, 13, 12, 4, 4, 14, 1, 6, 18, 2, 6, 16, 0, 9, 10, 7, 12, 3])

puts hh([1, 1])

puts hh([1])

puts hh([])
